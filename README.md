# What is it?
This is **pic**, the image hosting service

# How to run?
1. ```docker-compose build```
2. ```docker-compose up``` 

# How to use?
Make requests on port `8080`  
`GET`    `/` - check status  
`POST` `/upload` - upload an image  
`GET` `/{image_id}` - download an image  
`DELETE` `/{image_id}` - remove stored image  
`GET` `/{user_ip_address}` - get all user's uploaded image metadata  
`DELETE` `/{user_ip_address}` - remove all user's files and metadata

# Important note
To check your user IP address, please check logs when uploading image.
