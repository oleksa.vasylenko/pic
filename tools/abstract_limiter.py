from abc import ABCMeta, abstractmethod
from typing import Any


class AbstractLimiter(metaclass=ABCMeta):
    @abstractmethod
    def remaining_calls(self) -> int:
        pass

    @abstractmethod
    def __call__(self, cls: Any):
        pass
