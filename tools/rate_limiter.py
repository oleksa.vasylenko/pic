import asyncio
import functools
import logging

from typing import Any

_logger = logging.getLogger()


class _RateLimiter:

    def __init__(self, max_calls: int, period: int = 1, raise_on_limit: Exception = None):
        self._max_calls = max_calls
        self._calls = 0
        self._period = period
        self._lock = asyncio.Lock()
        if raise_on_limit is None:
            raise_on_limit = Exception('limit exceeded')
        self._limit_exc = raise_on_limit
        self.ticker = asyncio.ensure_future(self._start_ticker())

    @property
    def remaining_calls(self) -> int:
        return self._max_calls - self._calls

    async def _start_ticker(self) -> None:
        while True:
            await asyncio.sleep(self._period)
            async with self._lock:
                self._calls = 0

    def __call__(self, obj: Any):
        def track_qps(coroutine):
            @functools.wraps(coroutine)
            async def decorated(*args, **kwargs):
                async with self._lock:
                    self._calls += 1
                    if self._calls > self._max_calls:
                        raise self._limit_exc
                return await coroutine(*args, **kwargs)
            return decorated

        for attr_name in dir(obj):
            if not attr_name.startswith("_"):
                attr = getattr(obj, attr_name, None)
                if callable(attr):
                    coro = attr
                    setattr(obj, attr_name, track_qps(coro))
        return obj


def init_rate_limiter(qps: int,
                      period_sec: int = 1,
                      raise_on_limit: BaseException = None) -> _RateLimiter:
    return _RateLimiter(qps, period_sec, raise_on_limit)
