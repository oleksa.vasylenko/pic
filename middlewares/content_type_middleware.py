from aiohttp import web

from typing import Any

from constants import SUPPORTED_CONTENT_TYPES


@web.middleware
async def content_type_middleware(req: web.Request, handler: Any) -> web.Response:
    if req.method == 'POST':
        c_type = req.headers.get('Content-Type')
        if c_type is None or c_type not in SUPPORTED_CONTENT_TYPES:
            raise web.HTTPUnsupportedMediaType
    return await handler(req)
