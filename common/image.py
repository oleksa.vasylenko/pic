from dataclasses import dataclass
from datetime import datetime
from typing import Dict, Any


@dataclass
class Image:
    content_type: str
    data: bytes = b''
    id: str = ""
    upload_date: datetime = None
    size: int = 0

    def to_dict(self) -> Dict[str, Any]:
        result = {}
        if self.id:
            result['id'] = self.id
        if self.content_type:
            result['content_type'] = self.content_type
        if self.size:
            result['size'] = self.size
        if self.upload_date:
            result['upload_date'] = self.upload_date.isoformat()
        return result


