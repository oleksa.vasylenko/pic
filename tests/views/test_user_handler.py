import json
from datetime import datetime, timezone
from unittest.mock import patch

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from common.image import Image
from main import setup_app
from routes import setup_routes
from tests import AsyncMock
from tests.views import get_db_mock


class TestImageHandler(AioHTTPTestCase):
    user_id = '188.163.5.172'
    user_url = '/' + user_id

    async def get_application(self):
        app = setup_app()
        mocked_db = get_db_mock()
        app['image_db'] = mocked_db
        self.db = mocked_db
        setup_routes(app)
        return app

    @unittest_run_loop
    async def test_get_user_image_success(self):
        expected_id = '57c9e1df-0e26-4648-9a98-dac1ec476c5c'
        expected_date = datetime(2019, 5, 27, 7, 36, 6, 0, tzinfo=timezone.utc)
        expected_type = 'image/jpeg'
        expected_size = 1024

        with patch.object(self.db, 'list_files', new_callable=AsyncMock) as mock_list_files:
            mock_list_files.return_value = [Image(id=expected_id,
                                                  upload_date=expected_date,
                                                  content_type=expected_type,
                                                  size=expected_size)]

            response = await self.client.request('GET', self.user_url)
            body = json.loads(await response.read())

            self.assertEqual(200, response.status)
            self.assertEqual(expected_id, body[0]['id'])
            self.assertEqual(expected_size, body[0]['size'])
            self.assertEqual(expected_type, body[0]['content_type'])
            self.assertEqual(expected_date.isoformat(), body[0]['upload_date'])
            self.assertTrue(len(body[0]) == 4)
            self.assertTrue(len(body) == 1)

    @unittest_run_loop
    async def test_get_user_image_no_user(self):
        expected_body = b''
        with patch.object(self.db, 'list_files', new_callable=AsyncMock, return_value=[]):
            response = await self.client.request('GET', self.user_url)
            body = await response.read()

            self.assertEqual(404, response.status)
            self.assertEqual(expected_body, body)

    @unittest_run_loop
    async def test_get_user_image_no_user(self):
        expected_body = b''
        with patch.object(self.db, 'list_files', new_callable=AsyncMock, return_value=[]):
            response = await self.client.request('GET', self.user_url)
            body = await response.read()

            self.assertEqual(404, response.status)
            self.assertEqual(expected_body, body)

    @unittest_run_loop
    async def test_delete_user_images_success(self):
        expected_user_id = self.user_id
        expected_status = 'success'
        with patch.object(self.db, 'remove_all_files', new_callable=AsyncMock, return_value=True):
            response = await self.client.request('DELETE', self.user_url)
            body = json.loads(await response.read())

            self.assertEqual(200, response.status)
            self.assertEqual(expected_status, body['status'])
            self.assertEqual(self.user_id, body['id'])

    @unittest_run_loop
    async def test_delete_user_images_no_user(self):
        expected_body = b''
        with patch.object(self.db, 'remove_all_files', new_callable=AsyncMock, return_value=False):
            response = await self.client.request('DELETE', self.user_url)
            body = await response.read()

            self.assertEqual(404, response.status)
            self.assertEqual(expected_body, body)

