from unittest.mock import MagicMock

from storage.mongo_image_storage import MongoImageStorage


def get_db_mock():
    return MagicMock(autospec=MongoImageStorage, name='MockedImageDB')
