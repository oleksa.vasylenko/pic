import json
from unittest.mock import patch

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from common.image import Image
from main import setup_app

from routes import setup_routes
from tests import AsyncMock
from tests.views import get_db_mock


class TestImageHandler(AioHTTPTestCase):
    image_id = '57c9e1df-0e26-4648-9a98-dac1ec476c5c'
    image_url = '/' + image_id

    async def get_application(self):
        app = setup_app()
        mocked_db = get_db_mock()
        app['image_db'] = mocked_db
        self.db = mocked_db
        setup_routes(app)
        return app

    @unittest_run_loop
    async def test_get_image_200(self):
        expected = Image(data=b'some_image', content_type='image/jpeg')
        with patch.object(self.db, 'load_file', new_callable=AsyncMock) as load_file:
            load_file.return_value = expected

            response = await self.client.request("GET", self.image_url)
            content = await response.content.read()

            self.assertEqual(200, response.status)
            self.assertEqual(expected.data, content)

    @unittest_run_loop
    async def test_get_image_404(self):
        expected = b''
        with patch.object(self.db, 'load_file', new_callable=AsyncMock) as load_file:
            load_file.return_value = None

            response = await self.client.request("GET", self.image_url)
            content = await response.content.read()

            self.assertEqual(404, response.status)
            self.assertEqual(expected, content)

    @unittest_run_loop
    @patch('storage.mongo_image_storage.uuid.uuid4')
    async def test_upload_success(self, mock_uuid):
        image = b"some_image"
        expected_id = '30638168-22b2-4870-948d-e0f96d0c66a2'
        mock_uuid.return_value = expected_id
        with patch.object(self.db, 'save_file', new_callable=AsyncMock) as save_file:
            save_file.return_value = expected_id

            response = await self.client.request("POST", '/upload',
                                                 data=image,
                                                 headers={'content-type': 'image/jpeg'})
            body = json.loads(await response.read())

            self.assertEqual(201, response.status)
            self.assertEqual('success', body['status'])
            self.assertEqual(expected_id, body['id'])
            self.assertTrue(len(body) == 2)

    @unittest_run_loop
    async def test_upload_empty_body(self):
        image = b""
        with patch.object(self.db, 'save_file', new_callable=AsyncMock):

            response = await self.client.request("POST", '/upload',
                                                 data=image,
                                                 headers={'content-type': 'image/jpeg'})
            self.assertEqual(400, response.status)

    @unittest_run_loop
    async def test_delete_image_successful(self):
        expected_id = self.image_id
        with patch.object(self.db, 'remove_file', new_callable=AsyncMock, return_value=True):
            response = await self.client.request("DELETE", self.image_url)
            body = json.loads(await response.read())

            self.assertEqual(200, response.status)
            self.assertEqual('success', body['status'])
            self.assertEqual(expected_id, body['id'])
            self.assertTrue(len(body) == 2)

    @unittest_run_loop
    async def test_delete_image_no_image(self):
        with patch.object(self.db, 'remove_file', new_callable=AsyncMock, return_value=False):
            response = await self.client.request("DELETE", self.image_url)

            self.assertEqual(404, response.status)
