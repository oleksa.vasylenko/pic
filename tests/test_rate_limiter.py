import asyncio
from contextlib import suppress
from unittest import TestCase
from unittest.mock import patch

from tests import AsyncMock
from tools.rate_limiter import _RateLimiter


class Mockable():
    async def do_nothing(self):
        pass


class TestRateLimiter(TestCase):
    class LimitExceeded(Exception):
        pass

    def setUp(self):
        self.loop = asyncio.get_event_loop()

    def tearDown(self):
        pending = asyncio.all_tasks(self.loop)
        for task in pending:
            if not task.cancelled():
                task.cancel()
        with suppress(asyncio.CancelledError):
            tasks = asyncio.gather(*pending, loop=self.loop)
            self.loop.run_until_complete(tasks)
        self.loop.close()

    def test_decorated_object_limiting(self):
        expected_actions = 3

        rate_limiter = _RateLimiter(expected_actions, 1, raise_on_limit=self.LimitExceeded())
        with patch.object(Mockable, 'do_nothing', new_callable=AsyncMock) as do_nothing:

            mock = rate_limiter(Mockable())

            with self.assertRaises(self.LimitExceeded):
                for i in range(expected_actions+1):
                    self.loop.run_until_complete(mock.do_nothing())
            self.assertEqual(expected_actions, do_nothing.call_count)

