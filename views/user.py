from aiohttp import web

from storage.image_storage import ImageStorage


class UserHandler:
    def __init__(self, db: ImageStorage):
        self._db = db

    async def get_user_images(self, req: web.Request) -> web.Response:
        user_id = req.match_info.get('user_id')
        images = await self._db.list_files(user_id)
        if images:
            return web.json_response([image.to_dict() for image in images])
        return web.Response(status=404)

    async def delete_user_images(self, req: web.Request) -> web.Response:
        user_id = req.match_info.get('user_id')
        deleted = await self._db.remove_all_files(user_id)
        if deleted:
            return web.json_response({
                "status": "success",
                "id": user_id})
        return web.Response(status=404)
