from aiohttp import web
from aiohttp.web_exceptions import HTTPServiceUnavailable


async def status(req: web.Request) -> web.Response:
    limiter = req.app['rate_limiter']
    qps_left = limiter.remaining_calls
    if qps_left <= 0:
        raise HTTPServiceUnavailable

    return web.json_response({"status": "ok"}, headers={
        "Ratelimit-Remaining": str(qps_left)
    })
