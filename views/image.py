import logging

from aiohttp import web

from storage.image_storage import ImageStorage

_logger = logging.getLogger(__name__)

class ImageHandler:
    def __init__(self, db: ImageStorage):
        self._db = db

    async def upload(self, req: web.Request) -> web.Response:
        # todo: improve this approach
        host, _ = req.transport.get_extra_info('peername')
        _logger.info(f"Uploading image for {host}")
        body = await req.read()
        c_type = req.headers['Content-Type']
        if body:
            file_id = await self._db.save_file(owner_id=host, file=body, content_type=c_type)
            if file_id is not None:
                return web.json_response({"status": "success", "id": file_id},
                                         status=201)
        return web.Response(status=400)

    async def get_image(self, req: web.Request) -> web.Response:
        file_id = req.match_info.get('file_id')
        image = await self._db.load_file(file_id)
        if image is not None:
            return web.Response(body=image.data, content_type=image.content_type)

        return web.Response(status=404)

    async def delete_image(self, req: web.Request) -> web.Response:
        file_id = req.match_info.get('file_id')
        deleted = await self._db.remove_file(file_id)
        if deleted:
            return web.json_response({
                "status": "success",
                "id": file_id})
        return web.Response(status=404)
