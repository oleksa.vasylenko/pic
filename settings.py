import pathlib
import yaml
import os

from typing import Dict, Any

from constants import DEFAULT_CONFIG

config_name = os.getenv("ENV_CONFIG", DEFAULT_CONFIG)

BASE_DIR = pathlib.Path(__file__).parent
config_path = BASE_DIR / 'config' / f'{config_name}.yaml'


def _get_config(path: str) -> Dict[str, Any]:
    with open(path) as f:
        config = yaml.safe_load(f)
    return config


config = _get_config(config_path)
