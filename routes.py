from aiohttp import web

from views import image, user, status
from constants import REGEX_IP, REGEX_UUID


def setup_routes(app: web.Application) -> None:
    storage = app['image_db']

    user_handler = user.UserHandler(storage)
    image_handler = image.ImageHandler(storage)

    app.add_routes([
        web.get('/', status.status),
        web.get(r'/{user_id:'+REGEX_IP+'}', user_handler.get_user_images),
        web.delete(r'/{user_id:'+REGEX_IP+'}', user_handler.delete_user_images),
        web.get(r'/{file_id:'+REGEX_UUID+'}', image_handler.get_image),
        web.delete(r'/{file_id:'+REGEX_UUID+'}', image_handler.delete_image),
        web.post('/upload', image_handler.upload)
    ])

