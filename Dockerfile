FROM python:3.7.2-alpine


WORKDIR /usr/src/app

COPY requirements.txt ./
RUN apk update && apk add --no-cache python3-dev musl-dev gcc bash
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["python", "main.py"]