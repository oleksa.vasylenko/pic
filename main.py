import logging

from aiohttp import web
from aiohttp.web_exceptions import HTTPServiceUnavailable

from routes import setup_routes
from middlewares import *
from settings import config
from storage.async_db import shutdown_db, get_async_db_connection
from storage.mongo_image_storage import MongoImageStorage
from tools.abstract_limiter import AbstractLimiter
from tools.rate_limiter import init_rate_limiter

logging.basicConfig(**config['log'])
logger = logging.getLogger(__name__)


def setup_app() -> web.Application:
    app = web.Application(logger=logger, middlewares=[
        content_type_middleware
    ])

    return app


def setup_rate_limiter(app: web.Application) -> AbstractLimiter:
    qps = config['db']['rate_limit']
    limiter = init_rate_limiter(qps, raise_on_limit=HTTPServiceUnavailable())
    app['rate_limiter'] = limiter
    return limiter


def setup_db(app: web.Application) -> None:
    db = get_async_db_connection(config)
    db_limiter = setup_rate_limiter(app)

    storage = MongoImageStorage(db)
    app['image_db'] = db_limiter(storage)

    app.on_cleanup.append(shutdown_db)


def main() -> None:
    app = setup_app()
    setup_db(app)
    setup_routes(app)
    web.run_app(app)


if __name__ == '__main__':
    main()
