import logging

from aiohttp import web
from motor import motor_asyncio


_logger = logging.getLogger(__name__)

_db = None


def get_async_db_connection(config: dict) -> motor_asyncio.AsyncIOMotorDatabase:
    global _db
    if _db is None:
        db_config = config['db']
        _logger.info(f'Connecting to MongoDB: {db_config["url"]}')

        if not db_config.get('auth'):
            uri = 'mongodb://{url}'.format(**db_config)
        else:
            uri = 'mongodb://{user}:{pass}@{url}'.format(**db_config),

        client = motor_asyncio.AsyncIOMotorClient(uri, connectTimeoutMS=60000, connect=True)
        _db = client[db_config['name']]
    return _db


async def shutdown_db(_: web.Application) -> None:
    if _db is not None:
        _db.client.close()
