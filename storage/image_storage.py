from abc import ABCMeta, abstractmethod
from typing import Union, Optional

from common.image import Image


class ImageStorage(metaclass=ABCMeta):
    @abstractmethod
    async def save_file(self, owner_id: str, file: bytes, content_type: str) -> Union[str, None]:
        pass

    @abstractmethod
    async def load_file(self, file_id: str) -> Optional[Image]:
        pass

    @abstractmethod
    async def remove_file(self, file_id) -> bool:
        pass

    @abstractmethod
    async def list_files(self, owner_id: str):
        pass

    @abstractmethod
    async def remove_all_files(self, owner_id: str) -> bool:
        pass
