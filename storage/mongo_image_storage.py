import logging
import uuid
from typing import List, Union, Optional

from gridfs import NoFile
from motor.motor_asyncio import AsyncIOMotorGridFSBucket
from pymongo import DeleteMany, DeleteOne
from pymongo.errors import BulkWriteError

from common.image import Image
from storage.image_storage import ImageStorage

_logger = logging.getLogger(__name__)


class MongoImageStorage(ImageStorage):
    def __init__(self, db):
        self._db = db
        self._gfs = AsyncIOMotorGridFSBucket(db)

    async def save_file(self, owner_id: str, file: bytes, content_type: str, filename: str = "") -> Union[str, None]:
        image_id = str(uuid.uuid4())
        try:
            meta = {"owner": owner_id, "contentType": content_type}
            await self._gfs.upload_from_stream_with_id(image_id, filename, file, metadata=meta)
        except Exception as e:
            _logger.error(e)
            return None
        return image_id

    async def load_file(self, file_id: str) -> Optional[Image]:
        cursor = self._gfs.find({"_id": file_id})
        while await cursor.fetch_next:
            data = cursor.next_object()
            return Image(data=await data.read(),
                         content_type=data.metadata["contentType"])
        return None

    async def remove_file(self, file_id: str) -> bool:
        try:
            await self._gfs.delete(file_id)
        except NoFile:
            return False

        return True

    async def list_files(self, owner_id: str) -> List[Image]:
        cursor = self._gfs.find({"metadata.owner": owner_id})
        images = []
        while await cursor.fetch_next:
            data = cursor.next_object()
            images.append(Image(
                id=data._id,
                upload_date=data.upload_date,
                content_type=data.metadata["contentType"],
                size=data.length
            ))
        return images

    async def remove_all_files(self, owner_id: str) -> bool:
        cursor = self._db.fs.files.find({"metadata.owner": owner_id}, {"_id": 1})
        files_ops = []
        chunk_ops = []
        async for doc in cursor:
            file_id = doc["_id"]
            files_ops.append(DeleteOne({"_id": file_id}))
            chunk_ops.append(DeleteMany({"files_id": file_id}))
        if not files_ops and not chunk_ops:
            return False
        try:
            await self._db.fs.files.bulk_write(files_ops, ordered=False)
            await self._db.fs.chunks.bulk_write(chunk_ops, ordered=False)
        except BulkWriteError as bwe:
            _logger.error(bwe)
            return False

        return True
