
REGEX_UUID = r'\b[0-9a-f]{8}\-[0-9a-f]{4}\-4[0-9a-f]{3}\-[89ab][0-9a-f]{3}\-[0-9a-f]{12}\b'
REGEX_IP = r'\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b'

DEFAULT_CONFIG = 'local'

SUPPORTED_CONTENT_TYPES = (
    'image/jpeg',
    'image/png',
    'image/webp',
    'image/svg+xml',
    'image/gif',
    'image/webp',
    'image/pjpeg',
    'image/tiff',
    'image/vnd.microsoft.icon',
    'image/vnd.wap.wbmp'
)
